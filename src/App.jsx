import React,{useState,useEffect} from 'react';
import Input from './elements/Input.jsx'
import './elements/Input.css'

function App() {
    const [data,setData] = useState([])
    const [nombre,setNombre] = useState('')
    const [apellido,setApellido] = useState('')
    const [edad,setEdad] = useState('')
    function listar(){
        fetch('http://localhost:4000/persona', {
          method: 'get'
        }).then(res=>res.json())
          .then(res => {
            console.log(res.data)
            setData(res.data)
          });
    }
    useEffect(()=>{listar()},[])
    function guardar(){
        fetch('http://localhost:4000/persona', {
          method: 'post',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({nombre,apellido,edad})
        }).then(res=>res.json())
          .then(res => {
            setEdad('')
            setNombre('')
            setApellido('')
            listar()
            
          });
        
    }
    function borrar(e){
        fetch('http://localhost:4000/persona/'+e.target.getAttribute('persona'), {
          method: 'delete'
        }).then(res=>res.json())
          .then(res => {
            console.log(res)
            listar()
          });
    }
  return (
            <div>
                <h1>proyecto1</h1>
                <Input name="Nombre" value={nombre} onChange={setNombre}/>
                <Input name="Apellido" value={apellido} onChange={setApellido}/>
                <Input name="Edad" value={edad} onChange={setEdad}/>
                <button onClick={guardar}>Guardar</button>
                <button onClick={listar} >Listar</button>
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Edad</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map((val,i)=>{
                            return (
                                <tr key={val._id}>
                                    <td>{val._id}</td>
                                    <td>{val.nombre}</td>
                                    <td>{val.apellido}</td>
                                    <td>{val.edad}</td>
                                    <td>
                                        <button persona={val._id} onClick={borrar}>Borrar</button>
                                    </td>
                                </tr>
                            )
                        })}
                        
                    </tbody>
                </table>
                <ul>
                </ul>
            </div>
  );
}

export default App;
