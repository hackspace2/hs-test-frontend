import React from 'react'

function Input(props){
    let id = props.name.toLowerCase()
    function cambiar(e){
        props.onChange(e.target.value)
    }
    return (
            <div>
                <label htmlFor={id}>{props.name}</label>
                <br/>
                <input id={id} type="text" value={props.value} onChange={cambiar} />
                <br />
            </div>
        )
}

export default Input